from flask import Flask, render_template
import random
import redis

app = Flask(__name__)
r = redis.Redis(host='redisapp-service', port=6379, db=0)

# list of cat images
images = [
"Cat-Combing-Itself.gif",
"Cat-Hug.gif",
"Cat-Playing-Basketball.gif",
"Cat-Playing-Ping-Pong.gif",
"Cat-Using-Chopsticks.gif",
"Cat-Using-Computer.gif",
"Cat-Wearing-Glasses.gif",
"Cat-With-Beer.jpg",
"Cat-With-Teddy-Bear.gif",
"Cats-Sitting-Like-Human.gif",
"Cats-Using-iPad.gif",
"Cats-Wearing-Party-Hats.gif"
]

# initialize Redis with key / url
for i in images:
    output = r.set(i, "https://bitbucket.org/mirantis-training/public/raw/master/gifs/cats/"+str(i))
    print(str(i)+"set successfully: "+str(output)) 

@app.route('/')
def index():
    image = random.choice(images)
    url = r.get(image)
    url = url.decode("utf-8")
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
